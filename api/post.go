package handler

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/remotejob/cvvueweb/pkg/box"
	"gitlab.com/remotejob/cvvueweb/pkg/domains"
)

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	// (*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization,To-Do")
	(*w).Header().Set("Access-Control-Allow-Headers", "*")
}

func Post(w http.ResponseWriter, r *http.Request) {

	todo := r.Header.Get("To-Do")

	log.Println("todo", todo)

	setupResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	defer r.Body.Close()

	switch todo {

	case "jobs":

		var res domains.MyJobs
		json.Unmarshal(box.Get("/jobs.json"), &res)

		js, err := json.Marshal(res)
		if err != nil {
			log.Panicln(err)
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(js))

	case "knowlege":

		var res domains.MyKnowlege
		json.Unmarshal(box.Get("/knowlege.json"), &res)

		js, err := json.Marshal(res)
		if err != nil {
			log.Panicln(err)
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(js))

	}

}
