import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import '@fortawesome/fontawesome-free/css/all.css'
import App from './App.vue'
import store from './store/store'

import router from './router';

Vue.config.productionTip = false

Vue.use(Buefy, {
  defaultIconPack: 'fas'
})

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
