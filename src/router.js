import Vue from 'vue'
import VueRouter from 'vue-router'

import Knowlege from './components/Knowlege.vue';
import Jobs from './components/Jobs.vue';
import Details from './components/Details'
import Contact from '@/components/Contact'



Vue.use(VueRouter)


const routes = [{
    path: '/',
    name: 'Jobs',
    component: Jobs
  },
  {
    path: '/knowlege',
    name: 'Knowlege',
    component: Knowlege
  },
  {
    path: '/details',
    name: 'Details',
    component: Details
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router;