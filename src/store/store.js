import 'es6-promise/auto'
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

    restserver: process.env.VUE_APP_RESTSERVER,
    // restserver: 'api/post', 
    selectedItem: {},
    // joinmsgs: {},
    // nguid: "",
    // client: {},
    // lastmsg: {},
    // newmsg: {},
    // askmsg: '',
    // start: false,
    // history: {},
    // selecthistory:{},
    // host: ""

  },

  getters: {
//     getJoinMsg: state => {
//       return state.joinmsgs
//     },
//     getNguid: state => {
//       return state.nguid
//     },
//     getChatuuid: state => {
//       return state.selectedImg.Timeid
//     },
    getSelectedItem: state => {
      return state.selectedItem
    },
//     getStart: state => {
//       return state.start
//     },
//     getHistory: state => {
//       return state.history
//     },
//     getselectHistory: state => {
//       return state.selecthistory
//     }, 
//     getHost: state => {
//       return state.host
//     }, 
  },

  mutations: {

//     makeSelectedImg(state, img) {
//       state.selectedImg = img
//     },
//     makeStart(state, selected) {
//       state.selectedImg = selected
//       state.start = true

//     },
//     addjoinmsg(state, jmsg) {

//       state.joinmsgs = jmsg

//     },
//     setNguid(state, nguid) {

//       state.nguid = nguid
//     },
//     setLastmsg(state, msg) {

//       var ltime = new Date().toLocaleTimeString("fi-FI");
//       ltime = ltime.split(".");
//       ltime = ltime[0] + ":" + ltime[1];
//       msg.Localtime = ltime
//       state.lastmsg = msg
//     },

    setSelectedItem(state, item) {
      state.selectedItem = item
    },
//     endStart(state) {
//       state.start = false
//     },
//     makeHistory(state, img) {
//       state.history = img
//     },
//     selectHistory(state, img) {
//       state.selecthistory = img
//       state.selectedImg = img
//     },
//     setHost(state,host) {
//       state.host = host
    // }
    


  },

  actions: {

//     setSelectedImg(context, img) {

//       context.commit('makeSelectedImg', img)

//     },

//     setStart(context, selected) {

//       context.commit('makeStart', selected)

//     },
//     newJoin(context, jmsg) {

//       context.commit('addjoinmsg', jmsg)

//     },
//     setNguid(context, nguid) {
//       context.commit('setNguid', nguid)

//     },
//     setLastmgs(context, msg) {
//       context.commit('setLastmsg', msg)

//     },
    setSelectedItem(context, item) {
      context.commit('setSelectedItem', item)

    },
//     endStart(context) {
//       context.commit('endStart')

//     },
//     setHistory(context, img) {
//       context.commit('makeHistory', img)
//     },
//     selectHistory(context, img) {
//       context.commit('selectHistory', img)
//     },
//     setHost(context, host) {

//       context.commit('setHost', host)
//     }

  }
});